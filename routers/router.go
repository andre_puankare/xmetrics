package routers

import (
	"github.com/gorilla/mux"
)

// InitRoutes init application routes
func InitRoutes() *mux.Router {
	router := mux.NewRouter()
	router = SetAuthenticationRoutes(router)
	return router
}
