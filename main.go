package main

import (
	"flag"
	"fmt"


	"log"
	"os"
	"time"
	"net/http"

	"github.com/gorilla/mux"
	"github.com/codegangsta/negroni"

	"xmetrics/routers"
	"xmetrics/settings"
)


package main

import (

	"net/http"
)

var (
	debug        = flag.Bool("debug", false, "debug mode")
	listen       = flag.String("listen", ":8080", "listen address")
	readTimeout  = flag.Int("readTimeout", 10, "server read timeout")
	writeTimeout = flag.Int("writeTimeout", 10, "server write timeout")
)

func main() {
	flag.Parse()
	settings.Init()

	router := routers.InitRoutes()
	n := negroni.Classic()
	n.UseHandler(router)

	server := &http.Server{
		Addr:           *listen,
		ReadTimeout:    time.Duration((*readTimeout)) * time.Second,
		WriteTimeout:   time.Duration((*writeTimeout)) * time.Second,
		MaxHeaderBytes: 1 << 20,
		Handler:        n,
	}

	fmt.Printf("Serve on %s\n", *listen)
	log.Fatal(server.ListenAndServe())
}